var headerHeight;

function addItemActive() {
    const items = document.querySelectorAll('.header .menu-items .item')
    items.forEach(el => {
        el.addEventListener('click', () => {
            console.log('el: ', el.classList);
            items.forEach(nestedItem => {
                nestedItem.classList.remove('active');
            })
            el.classList.add('active');
        })
    })
}

window.onload = () => {
    addItemActive();
    headerHeight = document.querySelectorAll('.header')[0].offsetHeight;
}

function myFunction() {
    var x = document.getElementById("myLinks");
    if (x.style.display === "block") {
      x.style.display = "none";
    } else {
      x.style.display = "block";
    }
  }